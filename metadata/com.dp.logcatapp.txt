Categories:System
License:MIT
Web Site:
Source Code:https://github.com/darshanparajuli/LogcatReader
Issue Tracker:https://github.com/darshanparajuli/LogcatReader/issues

Auto Name:Logcat Reader
Summary:A simple app to view logs
Description:
Logcat Reader makes it easy to view and save the device logs.
.

Repo Type:git
Repo:https://github.com/darshanparajuli/LogcatReader.git

Build:1.2.0,14
    commit=v1.2.0
    subdir=app
    gradle=yes

Build:1.2.1,17
    commit=v1.2.1
    subdir=app
    gradle=yes
    prebuild=sed -i -e '/keystore/d' build.gradle

Build:1.2.2,18
    commit=v1.2.2
    subdir=app
    gradle=yes
    prebuild=sed -i -e '/keystore/d' build.gradle

Build:1.3.0,19
    commit=v1.3.0
    subdir=app
    gradle=yes
    prebuild=sed -i -e '/keystore/d' build.gradle

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:1.3.0
Current Version Code:19
